# Specify the provider and region
provider "aws" {
  region = "us-east-1"
}

# Create a VPC
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
}

# Create an internet gateway
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

# Create public subnet 1
resource "aws_subnet" "public" {
  cidr_block        = "10.0.0.0/24"
  vpc_id            = "${aws_vpc.default.id}"
  availability_zone = "us-east-1a"
  tags {
    Name = "Public Subnet 1"
  }
}

# Create public subnet 2
resource "aws_subnet" "public2" {
  cidr_block        = "10.0.10.0/24"
  vpc_id            = "${aws_vpc.default.id}"
  availability_zone = "us-east-1b"
  tags {
    Name = "Public Subnet 2"
  }
}

# Create private subnet 1 for ASG instances
resource "aws_subnet" "private1" {
  cidr_block        = "10.0.1.0/24"
  vpc_id            = "${aws_vpc.default.id}"
  availability_zone = "us-east-1a"
  tags {
    Name = "Private subnet 1"
  }
}

# Create private subnet 2 for ASG instances
resource "aws_subnet" "private2" {
  cidr_block        = "10.0.2.0/24"
  vpc_id            = "${aws_vpc.default.id}"
  availability_zone = "us-east-1b"
  tags {
    Name = "Private subnet 2"
  }
}

# Create private subnet 3 for RDS master
resource "aws_subnet" "private3" {
  cidr_block        = "10.0.3.0/24"
  vpc_id            = "${aws_vpc.default.id}"
  availability_zone = "us-east-1a"
  tags {
    Name = "Private subnet 3"
  }
}

# Create private subnet 4 for RDS Standby (Multi-AZ)
resource "aws_subnet" "private4" {
  cidr_block        = "10.0.4.0/24"
  vpc_id            = "${aws_vpc.default.id}"
  availability_zone = "us-east-1b"
  tags {
    Name = "Private subnet 4"
  }
}

# Assosiate public subnet with main route table
resource "aws_route_table_association" "public-to-main" {
  subnet_id      = "${aws_subnet.public.id}"
  route_table_id = "${aws_vpc.default.main_route_table_id}"
}

# Assosiate public subnet 2 with main route table
resource "aws_route_table_association" "public2-to-main" {
  subnet_id      = "${aws_subnet.public2.id}"
  route_table_id = "${aws_vpc.default.main_route_table_id}"
}

# Create security group for instances in ASG
resource "aws_security_group" "default" {
  name        = "ASG instance security group"
  description = "Used in the terraform"
  vpc_id      = "${aws_vpc.default.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  # ELB to ASG Go app
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = ["${aws_security_group.elb.id}"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create security group for CI/CD instance
resource "aws_security_group" "ci" {
  name        = "CI/CD instance security group"
  description = "Used in the terraform"
  vpc_id      = "${aws_vpc.default.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create security group for memcached
resource "aws_security_group" "memcached" {
  name        = "memcached security group"
  description = "Used in the terraform"
  vpc_id      = "${aws_vpc.default.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 11211
    to_port     = 11211
    protocol    = "tcp"
    security_groups= ["${aws_security_group.default.id}", "${aws_security_group.ci.id}"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create security group for nat instance
resource "aws_security_group" "nat" {
  name        = "vpc_nat"
  description = "Allow traffic to pass from the private subnet to the internet"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups= ["${aws_security_group.default.id}", "${aws_security_group.ci.id}"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups = ["${aws_security_group.default.id}", "${aws_security_group.ci.id}"]
  }

  ingress {
    from_port   = 11371
    to_port     = 11371
    protocol    = "tcp"
    security_groups = ["${aws_security_group.default.id}", "${aws_security_group.ci.id}"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 11371
    to_port     = 11371
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${aws_vpc.default.id}"

  tags {
    Name = "NATSG"
  }
}

#Create NAT instance
resource "aws_instance" "nat" {
  ami                         = "ami-d4c5efc2"
  availability_zone           = "us-east-1a"
  instance_type               = "t2.micro"
  key_name                    = "eugene.semirski"
  vpc_security_group_ids      = ["${aws_security_group.nat.id}"]
  subnet_id                   = "${aws_subnet.public.id}"
  associate_public_ip_address = true
  source_dest_check           = false
  user_data                   = "${file("nat_data")}"

  tags {
    Name = "NAT instance"
  }
}

# Assosiate Elastic IP address with NAT instance
resource "aws_eip" "nat" {
  instance = "${aws_instance.nat.id}"
  vpc      = true
}

#Create route table for private subnets to reach the world
resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    instance_id = "${aws_instance.nat.id}"
  }

  tags {
      Name = "Private Subnet to NAT instance"
  }
}

# Assosiate private subnet 1 with private route table
resource "aws_route_table_association" "private1" {
  subnet_id = "${aws_subnet.private1.id}"
  route_table_id = "${aws_route_table.private.id}"
}

# Assosiate private subnet 2 with private route table
resource "aws_route_table_association" "private2" {
  subnet_id = "${aws_subnet.private2.id}"
  route_table_id = "${aws_route_table.private.id}"
}

# Assosiate private subnet 3 with private route table
resource "aws_route_table_association" "private3" {
  subnet_id = "${aws_subnet.private3.id}"
  route_table_id = "${aws_route_table.private.id}"
}

# Assosiate private subnet 4 with private route table
resource "aws_route_table_association" "private4" {
  subnet_id = "${aws_subnet.private4.id}"
  route_table_id = "${aws_route_table.private.id}"
}

# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "elb" {
  name        = "elb"
  description = "Used in the terraform"
  vpc_id      = "${aws_vpc.default.id}"

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
#    security_groups= ["${aws_security_group.default.id}"]
  }
}

# Create Elastic Load Balancer
resource "aws_elb" "elb" {
  name                = "elb"

  security_groups     = ["${aws_security_group.elb.id}"]
  subnets             = ["${aws_subnet.public.id}", "${aws_subnet.public2.id}"]

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 10
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:8080/"
    interval            = 30
  }
}

# Create RDS security group
resource "aws_security_group" "rds" {
  name        = "rds_sg"
  description = "Allow inbound traffic"
  vpc_id      = "${aws_vpc.default.id}"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    security_groups= ["${aws_security_group.default.id}", "${aws_security_group.nat.id}", "${aws_security_group.ci.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "RDS SG"
  }
}

# Create RDS subnet group
resource "aws_db_subnet_group" "rds" {
  name        = "rds_subnet_group"
  description = "RDS group of subnets"
  subnet_ids  = ["${aws_subnet.private3.id}", "${aws_subnet.private4.id}"]
}

# Create RDS instance (Multi-AZ)
resource "aws_db_instance" "default" {
  depends_on             = ["aws_security_group.rds"]
  identifier             = "mydb-rds"
  allocated_storage      = "5"
  engine                 = "mysql"
  engine_version         = "5.7.17"
  multi_az               = true
  backup_window          = "01:00-02:00"
  instance_class         = "db.t2.micro"
  name                   = "mydb"
  username               = "dbuser"
  password               = "0xNyNSJ&0Y0g"
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
  db_subnet_group_name   = "${aws_db_subnet_group.rds.id}"
  skip_final_snapshot    = true
}

# Create elasticache subnet group
resource "aws_elasticache_subnet_group" "memcached" {
  name       = "tf-elasticache-subnet"
  subnet_ids = ["${aws_subnet.private1.id}", "${aws_subnet.private2.id}"]
}

# Create memcached instances
resource "aws_elasticache_cluster" "default" {
  cluster_id           = "memcached-1"
  engine               = "memcached"
  node_type            = "cache.t2.micro"
  port                 = 11211
  num_cache_nodes      = 2
  parameter_group_name = "default.memcached1.4"
  availability_zones   = ["us-east-1a", "us-east-1b"]
  security_group_ids   = ["${aws_security_group.memcached.id}"]
  subnet_group_name    = "${aws_elasticache_subnet_group.memcached.id}"
}

#Create CI/CD instance
resource "aws_instance" "ci" {
  ami                         = "ami-d15a75c7"
  availability_zone           = "us-east-1a"
  instance_type               = "t2.micro"
  key_name                    = "eugene.semirski"
  vpc_security_group_ids      = ["${aws_security_group.ci.id}"]
  subnet_id                   = "${aws_subnet.private1.id}"
  associate_public_ip_address = false
  source_dest_check           = false
  depends_on                  = ["aws_db_instance.default", "aws_elasticache_cluster.default"]
  user_data                   = "${file("ci_data")}"

  tags {
    Name = "CI/CD instance"
  }
}

# Create a repository in ECR
resource "aws_ecr_repository" "jobtest" {
  name = "jobtest"
}

# Create policy for the repository
resource "aws_ecr_repository_policy" "jobtest" {
  repository = "${aws_ecr_repository.jobtest.name}"

  policy = <<EOF
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "statement 1",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeRepositories",
                "ecr:GetRepositoryPolicy",
                "ecr:ListImages",
                "ecr:DescribeImages",
                "ecr:DeleteRepository",
                "ecr:BatchDeleteImage",
                "ecr:SetRepositoryPolicy",
                "ecr:DeleteRepositoryPolicy"
            ]
        }
    ]
}
EOF
}


## Creates launch configuration
resource "aws_launch_configuration" "lc" {
  name                        = "lc"
  associate_public_ip_address = false
  ebs_optimized               = false
  enable_monitoring           = false
  image_id                    = "ami-d15a75c7"
  instance_type               = "t2.micro"
  key_name                    = "eugene.semirski"
  security_groups             = ["${aws_security_group.default.id}"]
  user_data                   = "${file("user_data")}"

  root_block_device {
    delete_on_termination = true
    volume_size           = "8"
    volume_type           = "gp2"
  }
}

## Creates auto scaling group
resource "aws_autoscaling_group" "asg" {
  default_cooldown          = "300"
  desired_capacity          = "2"
  health_check_grace_period = "300"
  health_check_type         = "ELB"
  launch_configuration      = "${aws_launch_configuration.lc.id}"
  max_size                  = "2"
  min_size                  = "2"
  name                      = "asg"
  vpc_zone_identifier       = ["${aws_subnet.private1.id}", "${aws_subnet.private2.id}"]
  load_balancers            = ["${aws_elb.elb.id}"]
}

package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"
import "net/http"
//import "github.com/integralist/go-elasticache/elasticache"


var db *sql.DB
var err error

func newPage(res http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.ServeFile(res, req, "./views/new.html")
		return
	}

	quote := req.FormValue("quote")
	category := req.FormValue("category")

	var q string

	err := db.QueryRow("SELECT quote FROM quote WHERE quote=?", quote).Scan(&q)

	switch {
	case err == sql.ErrNoRows:
		_, err = db.Exec("INSERT INTO quote(quote, category) VALUES(?, ?)", quote, category)
		if err != nil {
			http.Error(res, "Server error, unable to create your quote.", 500)
			return
		}

		res.Write([]byte("Quote created!"))
		return
	case err != nil:
		http.Error(res, "Server error, unable to create your quote.", 500)
		return
	default:
		http.Redirect(res, req, "/", 301)
	}
}

func quotePage(res http.ResponseWriter, req *http.Request) {

        var dbq string

        err := db.QueryRow("SELECT quote FROM quote AS a JOIN (SELECT CEIL(RAND()*( SELECT MAX(id) FROM quote)) AS id) AS b WHERE a.id=b.id AND ( quote!='from_cash' OR ( SELECT COUNT(*) FROM quote)=1 ) LIMIT 1").Scan(&dbq)
        if err != nil {
                http.Redirect(res, req, "/", 301)
                return
        }

        res.Write([]byte(dbq))

}

func homePage(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, "./views/index.html")
}

func main() {
	db, err = sql.Open("mysql", "dbuser:0xNyNSJ&0Y0g@tcp(mydb-rds.cchb4w4ijsm1.us-east-1.rds.amazonaws.com:3306)/mydb")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	http.HandleFunc("/new", newPage)
        http.HandleFunc("/quote", quotePage)
	http.HandleFunc("/", homePage)
	http.ListenAndServe(":8080", nil)
}

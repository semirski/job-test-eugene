# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Copy the local package files to the container's workspace.
ADD job-test-eugene /go/bin/job-test-eugene
ADD views /go/bin/views

# Getting modules
RUN go get github.com/go-sql-driver/mysql

# Defining working directory
WORKDIR /go/bin

# Run the compiled binary by default when the container starts.
ENTRYPOINT /go/bin/job-test-eugene

# Document that the service listens on port 8080.
EXPOSE 8080


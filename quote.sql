CREATE TABLE `quote` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `quote` VARCHAR(250) NULL DEFAULT NULL,
  `category` VARCHAR(120) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci';

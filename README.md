# Site Reliability Engineer test

Your task is to create a realistic production environment on AWS. It will
host a sample application you will build throughout this test using
technologies from our stack.

This must include terraform to create Infrastructure-as-Code, Puppet to
provision the servers and Docker to run the application.

Please list references (ie. link to GitHub) to third-party libraries/modules
used.

The provided AWS credentials are limited but enable you to use the following
services:

 * S3
 * EC2 (Elastic Loadbalancing, Autoscaling)
 * RDS
 * Elastic Container Registry
 * ElastiCache
 * DynamoDB

Unfortunately you can only use IAM to manage SSH keys with this user. This
means you can't create any policies or instance profiles. Please provide a
short summary on its possible usage in this environment.

## Infrastructure

The servers used in this environment must be based on the current Ubuntu LTS
release.

Manage all resources in AWS using terraform to build a reliable and secure
environment for your application.

Use Puppet for the installation, configuration of software and managing
services required by your application.

The servers should be provisioned using a masterless puppet setup. We recommend
using cloud-init for this. You may distribute the puppet data using S3.

Keep the solution as simple as possible but apply best current practices.

## Application

Build a web application to host a "Quote database". It should provide two
endpoints:

* `/new` to be used to insert new data into the Quote database
* `/quote` to be used to fetch a random quote from the database

Here's a type we suggest for structuring the quote data:

    type Quote struct {
      quote     string
      category  string
    }

You can choose to do this in Go or Java. There's no need to create a frontend
for this application, but a simple web form to insert data can be helpful.

Sample data to use with the application is provided in the `sample-data` folder.
